<?php


/**
* @file
* Integrates CompraFácil's payment service, with allows payment with Multibanco and Payshop.
*/


/**
* Implementation of hook_menu().
*/
function uc_comprafacil_menu() {
  $items = array();
  
  $items['cart/comprafacil/complete'] = array(
  'title' => 'Encomenda completa',
  'page callback' => 'uc_comprafacil_complete',
  'access callback' => 'uc_comprafacil_completion_access',
  'type' => MENU_CALLBACK,
  );
  
  $items['cart/comprafacil/finalize'] = array(
  'title' => 'Order complete',
  'page callback' => 'uc_comprafacil_finalize',
  'access callback' => 'uc_comprafacil_completion_access',
  'type' => MENU_CALLBACK,
  );
  
  return $items;
}

/**
* Implementation of hook_init().
*/
function uc_comprafacil_init() {
  global $conf;
  $conf['i18n_variables'][] = 'uc_comprafacil_method_title';
  $conf['i18n_variables'][] = 'uc_comprafacil_checkout_button';
}

/**
* Make sure anyone can complete their CompraFácil orders.
*/
function uc_comprafacil_completion_access() {
  return TRUE;
}

/**
* Implementation of hook_ucga_display().
*/
function uc_comprafacil_ucga_display() {
  // Tell UC Google Analytics to display the e-commerce JS on the custom
  // order completion page for this module.
  if (arg(0) == 'cart' && arg(1) == 'comprafacil' && arg(2) == 'finalize') {
    return TRUE;
  }
}

/**
* Implementation of hook_form_alter().
*/
function uc_comprafacil_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);
    
    if ($order->payment_method == 'comprafacil') {
      drupal_add_css(drupal_get_path('module', 'uc_comprafacil') .'/uc_comprafacil.css');
      unset($form['submit']);
      $form['#prefix'] = '<table id="two-checkout-review-table"><tr><td>';
      $form['#suffix'] = '</td><td>'. drupal_get_form('uc_comprafacil_form', $order) .'</td></tr></table>';
    }
  }
}

/**
* Implementation of hook_payment_method().
*
* @see uc_payment_method_comprafacil()
  */
function uc_comprafacil_payment_method() {
  $path = base_path() . drupal_get_path('module', 'uc_comprafacil');
  
  $title = '<img src="'. $path .'/mb-logo.jpg" style="position: relative; top: 11px; margin-right: 7px;">';
  $title .= variable_get('uc_comprafacil_method_title', t('Pagamento por Multibanco'));
  
  $methods[] = array(
  'id' => 'comprafacil',
  'name' => t('CompraFácil'),
  'title' => $title,
  'review' => variable_get('uc_comprafacil_check', FALSE) ? t('Multibanco') : t('Multibanco'),
  'desc' => t('Referência para pagamento no Multibanco.'),
  'callback' => 'uc_payment_method_comprafacil',
  'weight' => 3,
  'checkout' => TRUE,
  'no_gateway' => TRUE,
  );
  
  return $methods;
}


/**
* Add CompraFácil settings to the payment method settings form.
*
* @see uc_comprafacil_payment_method()
  */
function uc_payment_method_comprafacil($op, &$arg1) {
  switch ($op) {
    case 'cart-details':
    if (variable_get('uc_comprafacil_check', FALSE)) {
      if ($_SESSION['pay_method'] == 'PS') {
        $sel[2] = ' selected="selected"';
      }
      else {
        $sel[1] = ' selected="selected"';
      }
      unset($_SESSION['pay_method']);
      $details = '<div class="form-item"><b>'. t('Esolha o tipo de pagamento:')
        .'</b> <select name="pay_method" class="form-select" id="edit-pay-method">'
      .'<option value="RM"'. $sel[1] .'>'. t('Multibanco') .'</option>'
      .'<option value="PS"'. $sel[2] .'>'. t('Payshop') .'</option></select></div>';
    }
    return $details;
    
    case 'cart-process':
    $_SESSION['pay_method'] = $_POST['pay_method'];
    return;
    
    case 'settings':
    $form['uc_comprafacil_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Utilizador'),
    '#description' => t('O seu utilizador para login no Backoffice.'),
    '#default_value' => variable_get('uc_comprafacil_username', ''),
    '#size' => 16,
    );
    $form['uc_comprafacil_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('A seu password para login no Backoffice.'),
    '#default_value' => variable_get('uc_comprafacil_password', ''),
    '#size' => 16,
    );
    $form['uc_comprafacil_check'] = array(
    '#type' => 'checkbox',
    '#title' => t('Permitir que os utilizadores escolham pagar com Multibanco ou Payshop.'),
    '#default_value' => variable_get('uc_comprafacil_check', FALSE),
    );
    $form['uc_comprafacil_method_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Payment method title'),
    '#default_value' => variable_get('uc_comprafacil_method_title', t('Pagamentos por Multibanco e Payshop:')),
    );
    return $form;
  }
}

/**
* Form to build the submission to CompraFácil.
*/
function uc_comprafacil_form($form_state, $order) {
  
  $country = uc_get_country_data(array('country_id' => $order->billing_country));
  if ($country === FALSE) {
    $country = array(0 => array('country_iso_code_3' => 'USA'));
  }
  
  $context = array(
  'revision' => 'formatted-original',
  'type' => 'order_total',
  'subject' => array(
  'order' => $order,
  ),
  );
  $options = array(
  'sign' => FALSE,
  'dec' => '.',
  'thou' => FALSE,
  );
  
  $data = array(
  'valor' => uc_price($order->order_total, $context, $options),
  'RefExterna' => $order->order_id,
  'nome' => substr($order->billing_first_name .' '. $order->billing_last_name, 0, 128),
  'morada' => substr($order->billing_street1, 0, 64) . 
  substr($order->billing_street2, 0, 64) . 
  substr($order->billing_city, 0, 64) . 
  substr($order->billing_city, 0, 64),
  'codPostal' => substr($order->billing_postal_code, 0, 16),
  'localidade' => uc_get_zone_code($order->billing_zone),
  'country' => $country[0]['country_iso_code_3'],
  'email' => substr($order->primary_email, 0, 64),
  'telefoneContacto' => substr($order->billing_phone, 0, 16),
  'origem' => 'net-drupal',
  'IDUserBackoffice' => '-1',
  );
  
  
  foreach ($data as $name => $value) {
    $form['ref'][$name] = array('#type' => 'value', '#value' => $value);
  }
  
  $form['ref']['#tree'] = TRUE;  
  $form['#redirect'] = 'cart/comprafacil/complete/' . $order->order_id;
  
  $form['submit'] = array(
  '#type' => 'submit',
  '#value' => variable_get('uc_comprafacil_checkout_button', t('Submit Order')),
  );
  
  return $form;
}

function uc_comprafacil_form_submit($form, &$form_state) {
  
  $form_state['values']['ref']['IDCliente'] = variable_get('uc_comprafacil_username', '');
  $form_state['values']['ref']['password'] =  variable_get('uc_comprafacil_password', '');
  $form_state['values']['ref']['NIF'] =' ';
  $form_state['values']['ref']['informacao'] =' ';
  
  $ref = uc_comprafacil_getreference($form_state['values']['ref']);
  
  $data = array(
  'order_id' => $form_state['values']['ref']['RefExterna'],
  'entity' => $ref['entity'],
  'value' => $ref['value'],
  'reference' => $ref['reference']
  );
  
  drupal_write_record('uc_comprafacil', $data);
  
  
}
function uc_comprafacil_getreference($parameters) {
  try {
    
    $wsURL = "https://hm.comprafacil.pt/SIBSClick/webservice/ClicksmsV4.asmx?WSDL";
    $client = new SoapClient($wsURL);        
    $res = $client->SaveCompraToBDValor2 ($parameters); 
    
    if ($res->SaveCompraToBDValor2Result) {
      $ref['entity'] = $res->entidade;
      $ref['value'] = number_format($res->valorOut, 2);
      $ref['reference'] = $res->referencia;
      $error = "";
      
      return $ref;
    }
    else {
      $error = $res->error;
      return FALSE;
    }
    
  }
  catch (Exception $e) {
    $error = $e->getMessage();
    return FALSE;
  }
  
}

function uc_comprafacil_getreferencestatus($parameters) {
  
  $parameters['IDCliente'] = variable_get('uc_comprafacil_username', '');
  $parameters['password'] =  variable_get('uc_comprafacil_password', '');
  
  try {
    
    $wsURL = "https://hm.comprafacil.pt/SIBSClick/webservice/ClicksmsV4.asmx?WSDL";
    $client = new SoapClient($wsURL);        
    $res = $client->getInfoCompra ($parameters); 
    
    if ($res->getInfoCompraResult) {
      $ref['pago'] = $res->pago;
      $ref['data'] = $res->dataUltimoPagamento;
      $error = "";
      
      return $ref;
    }
    else {
      $error = $res->error;
      return FALSE;
    }
    
  }
  catch (Exception $e) {
    $error = $e->getMessage();
    return FALSE;
  }
  
}
function uc_comprafacil_cron() {
  
  $result = db_query("SELECT o.order_id, c.reference FROM {uc_orders} o, {uc_comprafacil} c WHERE order_status = 'pending' AND o.order_id = c.order_id");
  
  while ($order = db_fetch_object($result)) {
    $ref_status = uc_comprafacil_getreferencestatus(array('referencia' => $order->reference));
    
    if ($ref_status['pago']) {
      $comment = 'Referência paga em ' . $ref_status['data'];
      uc_order_comment_save($order->order_id, 0, $comment, 'admin');
      uc_order_update_status($order->order_id, 'payment_received');
    }
    
  }
}
/**
* Implementation of hook_token_list(). (token.module)
  */
function uc_comprafacil_token_list($type = 'all') {
  if ($type == 'order' || $type == 'ubercart' || $type == 'all') {
    $tokens['order']['order-mb-entity'] = t('The MB Entity.');
    $tokens['order']['order-mb-reference'] = t('The MB Reference.');
    $tokens['order']['order-mb-value'] = t('The MB Value.');
  }
  
  return $tokens;
}

function uc_comprafacil_token_values($type, $object = NULL) {
  switch ($type) {
    case 'order':
    $order = $object;
    $refs = uc_comprafacil_getreference_order($order->order_id);
    $values['order-mb-entity'] = $refs['entity'];
    $values['order-mb-reference'] = $refs['reference'];
    $values['order-mb-value'] = $refs['value'];
  }
  
  return $values;
}

function uc_comprafacil_getreference_order($order = 0) {
  
  $sql = 'SELECT entity, reference, value FROM {uc_comprafacil} where order_id = %d';
  $result = db_query($sql, $order);
  $refs = db_fetch_array($result);
  
  return $refs;
  
}


function uc_comprafacil_complete($order = 0) {
  
  if (!is_numeric($order)) {
    // We will just show a standard "access denied" page in this case.
    return drupal_access_denied();
  }
  
  
  $order = uc_order_load($order);
  
  if ($order === FALSE || uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
    print t('An error has occurred during payment.  Please contact us to ensure your order has submitted.');
    exit();
  }
  
  $cart_id = uc_cart_get_id();
  
  $refs = uc_comprafacil_getreference_order($order->order_id);
  
  // Empty that cart...
  uc_cart_empty($cart_id);
  
  $path = base_path() . drupal_get_path('module', 'uc_comprafacil');
  $logo = '<img src="'. $path .'/mb-logo.jpg">';
  $msgrefs = '<strong>' . t('Entity:') . ' </strong>' . $refs['entity'] . '<br/>' . 
  '<strong>' . t('Reference:') . ' </strong>' . $refs['reference'] . '<br/>' .
  '<strong>' . t('Value:') . ' </strong>' . $refs['value'] . '€';
  
  drupal_set_message($logo . '<br/>' . $msgrefs);
  
  drupal_set_message(t('Your order will be processed as soon as your payment clears for the abobe Multibanco reference.'));
  uc_order_comment_save($order->order_id, 0, 'MB:' . $refs['entity'] . '/' . $refs['reference'] . '/' .  $refs['value'], 'admin');
  
  drupal_goto('cart/comprafacil/finalize/' . $order->order_id);
}

function uc_comprafacil_finalize($order = 0) {
  
  if (!is_numeric($order)) {
    // We will just show a standard "access denied" page in this case.
    return drupal_access_denied();
  }
  
  $order = uc_order_load($order);
  
  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');
  
  $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
  
  $page = variable_get('uc_cart_checkout_complete_page', '');
  
  if (!empty($page)) {
    drupal_goto($page);
  }
  
  return $output;
  
}